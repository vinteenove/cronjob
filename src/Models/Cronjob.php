<?php

namespace Cronjob\Models;

use LliureCore\Model;

class Cronjob extends Model
{
    protected static $primaryKey = 'id';
    protected static ?string $table = 'cronjob';

    const WAITING = '0';
    const READY = '1';
    const RUNNING = '2';
    const HORAS_CRON_TRAVAMENTO = '10';

    /**
     * @param $status
     * @return \LliureCore\Collection
     * @throws \Exception
     */
    static function findCronjobByStatus($status)
    {
        $table = static::getTable();
        return static::findMany('select * from ' . $table . ' WHERE status =? AND (next_execution < NOW() OR next_execution IS NULL)', $status);
    }

    /**
     * @param $status
     * @return \LliureCore\Collection
     * @throws \Exception
     */
    static function findOneCronjobByStatus($status)
    {
        $table = static::getTable();
        return static::findMany('select * from ' . $table . ' WHERE status =? AND (next_execution < NOW() OR next_execution IS NULL) ORDER BY last_execution ASC limit 1', $status);
    }

    /**
     * @return \LliureCore\Collection
     * @throws \Exception
     */
    public static function findStoped()
    {

        $time = new \DateTime( date('Y-m-d') );
        $time->sub( new \DateInterval( 'P1D' ) );

        $table = static::getTable();
        return static::findMany('SELECT p1.* 
                FROM '.$table.' p1
                INNER JOIN
                (
                    SELECT max(last_execution) MaxPostDate,  name,  status, description, `group`
                    FROM '.$table.'
                    WHERE is_active = "1"
                    GROUP BY `group`
                ) p2
                  ON p1.`group` = p2.`group`
                  AND p1.last_execution = p2.MaxPostDate
                  
                WHERE is_active = "1" AND p1.last_execution < ?
                order by p1.last_execution desc', $time->format('Y-m-d'));
    }
 }
