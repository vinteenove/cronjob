CREATE TABLE `ll_cronjob` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`group` INT(11) NOT NULL,
	`name` VARCHAR(100) NOT NULL COLLATE 'utf8_general_ci',
	`description` VARCHAR(200) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`path` VARCHAR(250) NOT NULL COLLATE 'utf8_general_ci',
	`status` INT(11) NOT NULL,
	`is_active` INT(11) NOT NULL DEFAULT '1',
	`attempts` INT(11) NULL DEFAULT '0',
	`id_next_cron` INT(11) NOT NULL DEFAULT '1',
	`interval` VARCHAR(4) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`next_execution` DATETIME NULL DEFAULT NULL,
	`last_execution` DATETIME NOT NULL,
	`email_alert_sent` TINYINT(4) NULL DEFAULT '0',
	PRIMARY KEY (`id`) USING BTREE,
	UNIQUE INDEX `cron_execution_id_uindex` (`id`) USING BTREE,
	UNIQUE INDEX `cron_execution_path_uindex` (`path`) USING BTREE,
	UNIQUE INDEX `cron_execution_name_uindex` (`name`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
