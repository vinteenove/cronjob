<?php

namespace Cronjob;

/**
 * Ferramentas de para Execução dos Crons
 *
 * Este arquivo consiste de um conjunto de funções para realizar:
 * Controle de estados de execução;
 * Avaliação de crons travados há muito tempo
 *
 * Requisito: Tabela 'cron_execution'
 *
 * Desenvolvimento:
 * Jonathan C. de Barros
 * Fevereiro de 2018.
 *
 * Jeison Frasson
 * Outubro de 2019
 *
 */

use Cronjob\Models\Cronjob as ModelCronjob;

class Cronjob
{
	protected static $SHARED_SERVER = false;
	
	/**
	 * @param bool $SHARED_SERVER
	 */
	public static function setSharedServer(bool $SHARED_SERVER)
	{
		self::$SHARED_SERVER = $SHARED_SERVER? true: false;
	}
	
	/**
	 * @param $cronId
	 * @param $newStatus
	 * @return void
	 */
	public static function changeCronStatus($cronId, $newStatus)
	{
		try{
			if(is_null($cronId)){
				throw new \Exception("Parâmetro (cronId) pendente na função de mudança de status.");
			}
			
			$cron = ModelCronjob::load($cronId);
			
			if(is_null($cron)){
				throw new \Exception("Parâmetro (cronId) não encontrou um cronjob corespondente.");
			}
			
			$cron['status'] = $newStatus;
			
			switch($newStatus){
				case ModelCronjob::WAITING: // step in which cron finishes and makes it available in waiting status
					
					// It means the cron has been reset
					$cron['attempts'] = 0;
					
					if($cron['interval'] !== null){
						$next = (new \DateTime())->add(new \DateInterval($cron['interval']));
						$cron['next_execution'] = $next->format("Y-m-d H:i");
					}
				
				break;
				case ModelCronjob::RUNNING: // step where you start the cron process by setting the status to running
					$now = new \DateTime();
					
					$cron['attempts'] = $cron['attempts'] + 1;
					$cron['last_execution'] = $now->format("Y-m-d H:i");
					
					//In case there was an alert sent, this flag is reset upon the cron running again
					$cron['email_alert_sent'] = 0;
				
				break;
			}
			
			$cron->update();
		}catch(\Throwable $e){
			//            echo $e->getMessage();
			die();
		}
	}
	
	/**
	 * @param $cronId
	 * @return false|mixed
	 * @throws \Exception
	 */
	public static function getIdNextCron($cronId)
	{
		try{
			$nextCron = ModelCronjob::load($cronId);
			return $nextCron['id_next_cron'];
		}catch(\Throwable $e){
			return false;
		}
	}
	
	/**
	 * @return bool
	 */
	public static function resetStuckCrons()
	{
		$crons = self::getCronsRunning();
		
		if(empty($crons)){
			return true;
		}
		
		foreach($crons as $cron){
			$lastExecution = new \DateTime($cron['last_execution']);
			$now = new \DateTime();
			$hoursOfDifference = $lastExecution->diff($now)->h;
			
			if($hoursOfDifference >= ModelCronjob::HORAS_CRON_TRAVAMENTO){
				self::stopAndResetCronExecution($cron['id']);
				
			}
		}
	}
	
	/**
	 * @return array|false
	 * @throws \Dibi\Exception
	 */
	public static function getCronsReadyToRun()
	{
		try{
			if(self::$SHARED_SERVER){
				$crons = ModelCronjob::findOneCronjobByStatus(ModelCronjob::READY);
			}else{
				$crons = ModelCronjob::findCronjobByStatus(ModelCronjob::READY);
			}
			
			if(empty($crons)){
				return [];
			}
			
			$activeAndReadyCrons = [];
			
			foreach($crons as $cron){
				if(!$cron->is_active){
					self::finishCronExecution($cron->id);
				}else{
					array_push($activeAndReadyCrons, $cron);
				}
			}
			
			return $activeAndReadyCrons;
			
		}catch(\Throwable $e){
			return false;
		}
	}
	
	/**
	 * @return array|false|\Lliure\Collection
	 */
	public static function getCronsRunning()
	{
		try{
			$crons = ModelCronjob::findCronjobByStatus(ModelCronjob::RUNNING);
			if(empty($crons)){
				return [];
			}
			
			return $crons;
			
		}catch(\Throwable $e){
			return false;
		}
	}
	
	/**
	 * @param $cronId
	 * It signals the start of the cron execution by changing its status and attempts in order
	 * to prevent duplicated execution of the same cron.
	 */
	public static function startCronExecution($cronId)
	{
		self::changeCronStatus($cronId, ModelCronjob::RUNNING);
	}
	
	/**
	 * @param $cronId
	 * @return bool
	 * In case of exception or unexpected behavior during its execution,
	 * this function reset the cron in order to be executed again.
	 */
	public static function stopAndResetCronExecution($cronId)
	{
		self::changeCronStatus($cronId, ModelCronjob::READY);
		return true;
	}
	
	/**
	 * @param $cronId
	 * @return bool
	 * @throws \Exception
	 * It signals the end of the cron execution and set the next one for execution.
	 */
	public static function finishCronExecution($cronId)
	{
		self::changeCronStatus($cronId, ModelCronjob::WAITING);
		self::changeCronStatus(self::getIdNextCron($cronId), ModelCronjob::READY);
		
		return true;
	}
}
