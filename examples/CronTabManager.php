<?php
require realpath(__DIR__ . '/../../vendor/autoload.php');
require realpath(__DIR__ . '/../../etc/bdconf.php');

set_time_limit(0);
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);

use Cronjob\Cronjob;

try {
    Cronjob::resetStuckCrons();
    $cronsReady = Cronjob::getCronsReadyToRun();

    if (empty($cronsReady)) {
        die();
    }

    foreach ($cronsReady as $cron) {
        if ($cron->attempts >= 3) {
                /*
                 *
                 * envio de e-mail para jobs travados
                 *
                 */
        } else {
            shell_exec("php " . __DIR__ . "/../../app/" . $cron->path . " > /dev/null 2>/dev/null &");
        }
    }
} catch (Exception $e) {
    echo $e->getMessage();
}
