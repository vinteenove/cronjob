<?php
require realpath(__DIR__ . '/../../vendor/autoload.php');
require realpath(__DIR__ . '/../../etc/bdconf.php');

set_time_limit(0);
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_erros', 1);

use Cronjob\Cronjob;

/*	start cron	*/
Cronjob::startCronExecution(1);


/*
*
*	code
*
*/

/*	reset cron	*/
if(false){
	Cronjob::stopAndResetCronExecution(1);
}



/*
*
*	code
*
*/

/*	finish cron	*/
Cronjob::finishCronExecution(1);